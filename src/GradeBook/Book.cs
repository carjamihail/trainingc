﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GradeBook
{
    public class Book
    {
        static private List<double> grades;
        public string Name;
        public Book(string name)
        {
            grades = new List<double>();
            Name = name;
        }
        public void AddGrade(double grade)
        {
           grades.Add(grade);
        }

        public Statistics GetStatisc()
        {
            var result = new Statistics();
             result.Average = 0.0;
             result.High = double.MinValue;
             result.Low = double.MaxValue;
            foreach (var x in grades)
            {
                result.Low = Math.Min(x, result.Low);
                result.High = Math.Max(x, result.High);
                result.Average += x;

            }
            result.Average /= grades.Count;
         
            return result;
        }

     
    }
}
