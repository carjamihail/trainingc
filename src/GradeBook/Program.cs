﻿using System;
using System.Collections.Generic;

namespace GradeBook
{   
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("asdf");
            book.AddGrade(89.1);
            book.AddGrade(90.5);
            book.AddGrade(77.3);
            var result = book.GetStatisc();
            Console.WriteLine($"Average {result.Average:N2}");
            Console.WriteLine($"Highest grade is {result.High}");
            Console.WriteLine($"Lowest grade is {result.Low}");

        }
    }
}
